import sys
from PyQt5.QtWidgets import *
from PyQt5.QAxContainer import *
import datetime


class MyWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Real")
        self.setGeometry(300, 300, 300, 400)

        btn = QPushButton("Register", self)
        btn.move(20, 20)
        btn.clicked.connect(self.btn_clicked)

        btn2 = QPushButton("DisConnect", self)
        btn2.move(20, 100)
        btn2.clicked.connect(self.btn2_clicked)

        self.stack_volume = 0
        self.kiwoom = QAxWidget("KHOPENAPI.KHOpenAPICtrl.1")
        self.kiwoom.OnEventConnect.connect(self._handler_login)
        self.kiwoom.OnReceiveRealData.connect(self._handler_real_data)
        self.CommmConnect()

    def btn_clicked(self):
        self.SetRealReg("1000", "005930", "20;10;15", 0)
        #self.SetRealReg("3000", "101QC000", "20;10;15", 0)


    def btn2_clicked(self):
        self.DisConnectRealData("1000")

    def CommmConnect(self):
        self.kiwoom.dynamicCall("CommConnect()")
        self.statusBar().showMessage("login 중 ...")

    def _handler_login(self, err_code):
        if err_code == 0:
            self.statusBar().showMessage("login 완료")


    def _handler_real_data(self, code, real_type, data):
        #if real_type == "선물시세":
        if real_type == "주식체결":
            # 체결 시간
            #print(real_type)
            time =  self.GetCommRealData(code, 20)
            date = datetime.datetime.now().strftime("%Y-%m-%d ")
            time =  datetime.datetime.strptime(date + time, "%Y-%m-%d %H%M%S")
            print(time, end=" ")

            # 현재가
            price =  self.GetCommRealData(code, 10)
            print(int(price))
            '''
            # 거래량
            print(int(price), end=" ")
            volume =  self.GetCommRealData(code, 15)
            #print(abs(volume), end=" ")
            v_volume = abs(int(volume))
            print(v_volume, end=" ")
            #print("hi")
            self.stack_volume = self.stack_volume + v_volume
            #print("bye")
            print(self.stack_volume)
            '''

    def SetRealReg(self, screen_no, code_list, fid_list, real_type):
        self.kiwoom.dynamicCall("SetRealReg(QString, QString, QString, QString)",
                                screen_no, code_list, fid_list, real_type)
        '''
        t_print = self.kiwoom.dynamicCall("SetRealReg(QString, QString, QString, QString)",
                              screen_no, code_list, fid_list, real_type)
        print(t_print)
        '''
    def DisConnectRealData(self, screen_no):
        self.kiwoom.dynamicCall("DisConnectRealData(QString)", screen_no)

    def GetCommRealData(self, code, fid):
        data = self.kiwoom.dynamicCall("GetCommRealData(QString, int)", code, fid)
        return data


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MyWindow()
    window.show()
    app.exec_()