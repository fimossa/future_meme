######################### API로 종목 분석하기 158-191

from PyQt5.QtCore import *
from PyQt5.QtTest import *

import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QAxContainer import *

from threading import Thread, Lock
import time
# import cx_Oracle as cx
import sys

sys.path.append("D://David\Documents//01_project//25_주식파이썬//17_automeme//")
from MeanMoveLine1 import MeanMoveLine
import pandas as pd
import traceback

from datetime import datetime

'''
전체 시나리오
당일매수매도 파워결정 알고리즘구현 : 실시간으로 변수저장
실시간 분봉 모니터링 및 변서로 저장(이평선 / 이격도 ) : 매수매도 결정

매수케이스 ( 종가 > 시가  & 3분봉 > 5분봉 & 5초이후 50초 이내 & 양봉 )
매수호가1확인 : 실시간변수로 저장
매수호가1로 주문
체결정보 수신 : 실시간변수로 저장

1틱 수익목표로 환매도 진행

체결 정보수신 (체결시 수익실현 완료)


※추가작업
틱 db저장



'''


class MyWindow(QMainWindow):
    infoDic = {}
    priCnt = '0'
    tot = 0
    jongmok = '101QC000' #종목코드
    acc = '0000000000' #계좌번호
    t_avr1 = 1
    t_avr2 = 3
    t_avr3 = 5

    t_quantity = 1
    cost = []
    guCnt = []
    sqlList = []
    timeDict = {}
    timeDictHo = {}
    mutex = Lock()

    rawData = {}
    rawDataMany = {}

    isBuy = False
    buyPrice = 0

    map = {}

    def __init__(self):


        super().__init__()
        # cx.init_oracle_client('C:/Users/yunmi/Downloads/instantclient-basic-nt-19.6.0.0.0dbru/instantclient_19_6')
        # dsn = cx.makedsn("localhost", 1521, service_name="master")
        #        self.conn = cx.connect("c##masteruser", "q7w5r1z8", dsn)
        self.setWindowTitle("PyStset Cock")
        self.setGeometry(300, 300, 300, 500)

        # self.detail_account_info_event_loop_f = QEventLoop()

        self.kiwoom = QAxWidget("KHOPENAPI.KHOpenAPICtrl.1")
        btn1 = QPushButton("Login", self)
        btn1.move(20, 20)
        btn1.clicked.connect(self.login)

        btn2 = QPushButton("매수", self)
        btn2.move(20, 60)
        btn2.clicked.connect(self.f_buy)

        btn3 = QPushButton("매도", self)
        btn3.move(20, 100)
        btn3.clicked.connect(self.f_sell)



        self.kiwoom.OnReceiveChejanData.connect(self.ChejangData)
        self.kiwoom.OnReceiveTrData.connect(self.ReceiveTrData)

    def login(self):
        self.kiwoom.dynamicCall("CommConnect()")
        print("로그인이 완료되었습니다")

    def ChejangData(self, sGunbun, sNItemCnt, sFidList):
        print("--------- 주식 체결 -----------")
        print(sGunbun)
        print(sNItemCnt)
        print(sFidList)
        if sGunbun == '0':
            '''
            Real Type : 주식체결
            [20] = 체결시간
            [10] = 현재가
            [11] = 전일대비
            [12] = 등락율
            [27] = (최우선)매도호가
            [28] = (최우선)매수호가
            [15] = 거래량
            [13] = 누적거래량
            [14] = 누적거래대금
            [16] = 시가
            [17] = 고가
            [18] = 저가
            [25] = 전일대비기호
            [26] = 전일거래량대비(계약,주)
            [29] = 거래대금증감
            [30] = 전일거래량대비(비율)
            [31] = 거래회전율
            [32] = 거래비용
            [228] = 체결강도
            [311] = 시가총액(억)
            [290] = 장구분
            [691] = KO접근도
            [567] = 상한가발생시간
            [568] = 하한가발생시간
            '''
            # ch = self.kiwoom.dynamicCall("GetChejanData(int)", [10])
            # print(ch)
        elif sGunbun == '4':
            '''
            Real Type : 잔고
            [9201] = 계좌번호
            [9001] = 종목코드,업종코드
            [917] = 신용구분
            [916] = 대출일
            [302] = 종목명
            [10] = 현재가
            [930] = 보유수량
            [931] = 매입단가
            [932] = 총매입가
            [933] = 주문가능수량
            [945] = 당일순매수량
            [946] = 매도/매수구분
            [950] = 당일총매도손일
            [951] = 예수금
            [27] = (최우선)매도호가
            [28] = (최우선)매수호가
            [307] = 기준가
            [8019] = 손익율
            [957] = 신용금액
            [958] = 신용이자
            [918] = 만기일
            [990] = 당일실현손익(유가)
            [991] = 당일실현손익률(유가)
            [992] = 당일실현손익(신용)
            [993] = 당일실현손익률(신용)
            [959] = 담보대출수량
            [924] = Extra Item
            '''
            # 종목코드 = self.kiwoom.dynamicCall("GetChejanData(int)", [9001])
            self.current_price = self.kiwoom.dynamicCall("GetChejanData(int)", [10])
            self.meme_price = self.kiwoom.dynamicCall("GetChejanData(int)", [931])
            self.q_wallet = int(self.kiwoom.dynamicCall("GetChejanData(int)", [930]))

            '''
            if len(종목코드) > 6:
                self.map[종목코드[1:7]] = 보유수량
            else:
                self.map[종목코드] = 보유수량
            '''
            # print(종목코드)
            print('현재가 : ', self.current_price, sep='     ')
            print('매입가 : ', self.meme_price, sep='     ')
            print('보유수량 : ', self.q_wallet, sep='     ')

    def ReceiveTrData(self, sScrNo, sRQName, sTRCode, sRecordName, sPreNext, nDataLength, sErrorCode, sMessage,
                      sSPlmMsg, **kwargs):
        # print("ReceiveTrData")
        try:
            print("ReveiveTrData")
            now = datetime.now()
            time = "{}-{}-{} {}:{}:{}".format(now.year, now.month, now.day, now.hour, now.minute, now.second)
            print(sTRCode)




        except Exception as e:
            print(e)
            print(traceback.format_exc())








    def f_buy(self):
        print(datetime.now())
        # 선물옵션 변수
        t_order = 1  # 주문종류  1:신규매매, 2:정정, 3:취소
        t_meme = 2  # 매매구분  1: 매도, 2:매수
        t_trade = '3'  # 시장가 거래구분(혹은 호가구분)은아래 참고
        t_quantity = 1  # 주문수량
        t_meme_price = "0"  # 주문가격
        t_order_num = ""

        # 사용자 구분 요청명 , 화면번호 , 계좌번호, 주문유형, 주식종목코드, 주문수량, 주문단가, 거래 구분, 원주문번호
        # ret = self.kiwoom.dynamicCall("SendOrder(QString,QString,QString,int,QString,int,int,QString,QString)",["RQ_1", "0101", self.acc, 1, self.jongmok, self.t_quantity, 0, "03",""])

        ret = self.kiwoom.dynamicCall(
            "SendOrderFO(Qstring,Qstring,Qstring,Qstring,int,Qstring,Qstring,int,Qstring,Qstring)",
            ["RQ_1", "0101", self.acc, self.jongmok, t_order, t_meme, t_trade, t_quantity,
             t_meme_price, t_order_num])
        # print(ret)
        print("매수주문")
        if ret != 0:
            print("주문실패")
            self.KiwoomProcessingError("SendOrder : " + ReturnCode.CAUSE[ret])
        else:
            print("주문성공")
        self.isBuy = True


    def f_sell(self):
        print(datetime.now())
        # 선물옵션 변수
        t_order = 1  # 주문종류  1:신규매매, 2:정정, 3:취소
        t_meme = 1  # 매매구분  1: 매도, 2:매수
        t_trade = '3'  # 거래구분(혹은 호가구분)은아래 참고
        t_quantity = 1  # 주문수량
        t_meme_price = 0  # 주문가격
        t_order_num = ""

        # 사용자 구분 요청명 , 화면번호 , 계좌번호, 주문유형, 주식종목코드, 주문수량, 주문단가, 거래 구분, 원주문번호
        # // 사용자구분명 // 화면번호 // 계좌번호 // 주문유형 1:신규매수, 2:신규매도 3:매수취소, 4:매도취소, 5:매수정정, 6:매도정정 // 종목코드 // 주문수량 // 주문가격 // 거래구분 // 원주문번호
        # ret = self.kiwoom.dynamicCall("SendOrder(QString,QString,QString,int,QString,int,int,QString,QString)",["RQ_1", "0101", self.acc, 2, self.jongmok, self.t_quantity, 0, "03", ""])
        # // 사용자구분명 // 화면번호 // 계좌번호  // 종목코드 // 주문종류  1:신규매매, 2:정정, 3:취소 // 매매구분  1: 매도, 2:매수 // 거래구분(혹은 호가구분)은아래 참고 // 주문수량 // 주문가격 // 원주문번호
        ret = self.kiwoom.dynamicCall(
            "SendOrderFO(Qstring,Qstring,Qstring,Qstring,int,int,Qstring,int,Qstring,Qstring)",
            ["RQ_1", "0101", self.acc, self.jongmok, t_order, t_meme, t_trade, t_quantity,
             t_meme_price, t_order_num])
        # print(ret)
        print("매도주문")
        if ret != 0:
            print("주문실패")
            self.KiwoomProcessingError("SendOrder : " + ReturnCode.CAUSE[ret])
        else:
            print("주문성공")
        self.isBuy = False


if __name__ == "__main__":
    app = QApplication(sys.argv)

    myWindow = MyWindow()
    # x = Thread(target=myWindow.dbInsertThread, args=())
    # x.daemon = True
    # x.start()
    myWindow.show()
    app.exec_()