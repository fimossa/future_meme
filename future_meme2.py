######################### API로 종목 분석하기 158-191

from PyQt5.QtCore import *
from PyQt5.QtTest import *



import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QAxContainer import *

from threading import Thread , Lock
import time
#import cx_Oracle as cx
import sys
sys.path.append("D://David\Documents//01_project//25_주식파이썬//17_automeme//")
from MeanMoveLine1 import MeanMoveLine
import pandas as pd
import traceback

from datetime import datetime


'''
전체 시나리오
당일매수매도 파워결정 알고리즘구현 : 실시간으로 변수저장
실시간 분봉 모니터링 및 변서로 저장(이평선 / 이격도 ) : 매수매도 결정

매수케이스 ( 종가 > 시가  & 3분봉 > 5분봉 & 5초이후 50초 이내 & 양봉 )
매수호가1확인 : 실시간변수로 저장
매수호가1로 주문
체결정보 수신 : 실시간변수로 저장

1틱 수익목표로 환매도 진행

체결 정보수신 (체결시 수익실현 완료)


※추가작업
틱 db저장



'''

class MyWindow(QMainWindow):
    infoDic = {}
    priCnt = '0'
    tot = 0
    jongmok = '101QC000'
    #acc = '8147055411'
    #acc = '5549734410'
    acc = '8750336831'
    t_avr1 = 1
    t_avr2 = 3
    t_avr3 = 5

    t_quantity = 1
    cost = []
    guCnt = []
    sqlList = []
    timeDict = {}
    timeDictHo={}
    mutex = Lock()

    rawData={}
    rawDataMany={}

    isBuy=False
    buyPrice=0

    map={}
    def __init__(self):

        self.day1 = '3'
        self.day2 = '5'
        self.day3 = '7'
        self.meme_price = ''
        self.current_price = ''
        self.price_delta = 0.05
        self.mesu1_price = ''
        self.medo1_price = ''
        self.q_wallet =  0
        self.chart_request_count = 20
        self.tic_chart_request_count = 900
        self.start_time = time.time()

        self.p_differentical_percent = 0.0
        self.p_average_price_delta = 0.0
        self.p_end_start_price_delta = 0.0
        self.p_average_price_slope = 0.0

        self.order_num = ""



        self.realData = pd.DataFrame(columns=['현재가', '거래량', '시가', '고가', '저가',self.day1,self.day2,self.day3])


        super().__init__()
        #cx.init_oracle_client('C:/Users/yunmi/Downloads/instantclient-basic-nt-19.6.0.0.0dbru/instantclient_19_6')
        #dsn = cx.makedsn("localhost", 1521, service_name="master")
#        self.conn = cx.connect("c##masteruser", "q7w5r1z8", dsn)
        self.setWindowTitle("PyStset Cock")
        self.setGeometry(300, 300, 300, 500)

        #self.detail_account_info_event_loop_f = QEventLoop()

        self.kiwoom = QAxWidget("KHOPENAPI.KHOpenAPICtrl.1")
        btn1 = QPushButton("Login", self)
        btn1.move(20, 20)
        btn1.clicked.connect(self.login)

        btn2 = QPushButton("Auto", self)
        btn2.move(20, 60)
        btn2.clicked.connect(self.auto)

        btn3 = QPushButton("잔고", self)
        btn3.move(20, 100)
        btn3.clicked.connect(self.remain)

        btn4 = QPushButton("선물차트", self)
        btn4.move(20, 140)
        btn4.clicked.connect(self.future_chart)

        btn5 = QPushButton("보유종목확인", self)
        btn5.move(20, 180)
        btn5.clicked.connect(self.asset_check)

        btn6 = QPushButton("매수", self)
        btn6.move(150, 20)
        btn6.clicked.connect(self.f_buy)

        btn7 = QPushButton("매도", self)
        btn7.move(150, 60)
        btn7.clicked.connect(self.f_sell)

        btn8 = QPushButton("매수호가1", self)
        btn8.move(150, 100)
        btn8.clicked.connect(self.current_price_call)

        btn9 = QPushButton("차트초당", self)
        btn9.move(150, 140)
        btn9.clicked.connect(self.future_chart_1s)

        btn10 = QPushButton("틱차트", self)
        btn10.move(150, 180)
        btn10.clicked.connect(self.future_tic_chart)

        btn11 = QPushButton("선물차트자동", self)
        #btn11.move(20, 220)
        btn11.setGeometry(20, 220, 230, 40)
        btn11.clicked.connect(self.future_chart_auto)

        btn12 = QPushButton("지정가매수", self)
        #btn11.move(20, 220)
        btn12.move(20, 300)
        btn12.clicked.connect(self.f_buy_fix_price)

        btn13 = QPushButton("주문취소", self)
        #btn11.move(20, 220)
        btn13.move(150, 300)
        btn13.clicked.connect(self.f_cancel)

        self.kiwoom.OnReceiveChejanData.connect(self.ChejangData)
        #self.kiwoom.OnReceiveRealData.connect(self.test3Real)
        self.kiwoom.OnReceiveTrData.connect(self.ReceiveTrData)
        #self.kiwoom.OnReceiveChejanData.connect(self._receive_chejan_data)


        #self.x1 = Thread(target=self.future_chart_1s_exe, args=())
        #self.x1.daemon = True
        #self.x1.start()


 #   def __del__(self):
 #       self.conn.close()


    def str_to_float(t_price):
        try:
            return round(float(t_price), 2)
        except:
            pass

    def get_chejan_data(self, fid):
        ret = self.dynamicCall("GetChejanData(int)", fid)
        return ret

    def get_chejan_data(self, fid):
        ret = self.kiwoom.dynamicCall("GetChejanData(int)", fid)
        return ret

    def _receive_chejan_data(self, gubun, item_cnt, fid_list):
        print(gubun)
        print(self.get_chejan_data(9203))
        print(self.get_chejan_data(302))
        print(self.get_chejan_data(900))
        print(self.get_chejan_data(901))

    def test3Real(self, sJongmokCode, sRealType, sRealData):
        self.x = Thread(target=self.th, args=(sJongmokCode, sRealType, sRealData))
        self.x.start()

    def th(self, sJongmokCode, sRealType, sRealData):
        tm = datetime.now()
        if sRealType == "주식체결":
            '''
            sp = sRealData.split('\t')
            if self.timeDict.keys().__contains__(sp[0]):
                self.timeDict[sp[0]]=self.timeDict[sp[0]]+1
            else:
                self.timeDict[sp[0]]=1

            cnt = self.timeDict[sp[0]]
            # 체결시간 / 현재가 / 전일대비 / 등락율 / 매도호가 / 매수호가 / 거개량 / 누적거래량 / 누적거래대금 / 시가 / 고가 / 저가 / 전일대비기호 / 전일거래량대비(계약,주) / 거래대금증감 / 전일거래량대비(비율) / 거래회전율 / 거래비용 / 체결강도 / 시가 총액 / 장구분 / KO접근도 / 상한가발생시간 / 하한가발생시간
            #print("시간: " +str(tm.date())+" "+ sp[0]+ " / 현재가: " + sp[1] + " / 전일대비:" + sp[2] + " / 등락율:" + sp[3] + " / 거래량:" + sp[6] + " / 매도수 거래량:" + str(self.tot))
            sql = 'INSERT INTO STOCK_REALDATA2 Values(\''+str(tm.date())+" "+ sp[0]+'\','+str(cnt)+','+sp[1]+','+sp[2]+','+sp[3]+','+sp[4]+','+sp[5]+','+sp[6]+','+sp[7]+','+sp[8]+','+sp[9]+','+sp[10]+','+sp[12]+','+sp[13]+','+sp[14]+','+sp[15]+','+sp[16]+','+sp[17]+','+sp[18]+','+sp[19]+','+sp[20]+','+sp[21]+','+sp[12]+',sysdate,'+self.jongmok+')'
            self.sqlList.append(sql)
            
            현재가 = sp[1].replace('+','').replace('-','')
            if sp[0][:4] in self.rawData.keys():
                self.rawData[sp[0][:4]].apopend(현재가)
                self.rawDataMany[sp[0][:4]] += int(sp[6])
            else:
                self.rawData[sp[0][:4]] = [현재가,]
                self.rawDataMany[sp[0][:4]]= int(sp[6])


            저가 = min(self.rawData[sp[0][:4]])
            고가 = max(self.rawData[sp[0][:4]])
            시가 = self.rawData[sp[0][:4]][0]
            거래량 = self.rawDataMany[sp[0][:4]]
            self.realData.loc[sp[0][:4],'현재가'] = 현재가
            self.realData.loc[sp[0][:4], '거래량'] = 거래량
            self.realData.loc[sp[0][:4], '시가'] = 시가
            self.realData.loc[sp[0][:4], '고가'] = 고가
            self.realData.loc[sp[0][:4], '저가'] = 저가

            info = MeanMoveLine(data=self.realData[:])
            print(info.moveLine)
            print(list(info.moveLine)[-1])
            print(list(info.moveLine.values())[-1])
            # info.getDeviation(info.list)
            res = info.currentBuyAndSell(list(info.moveLine.values())[-1], list(info.moveLine.values())[-2])
            print("res:", res)

            if res == 10:
                if self.isBuy == True:
                    # 사용자 구분 요청명 , 화면번호 , 계좌번호, 주문유형, 주식종목코드, 주문수량, 주문단가, 거래 구분, 원주문번호
                    ret = self.kiwoom.dynamicCall(
                        "SendOrder(QString,QString,QString,int,QString,int,int,QString,QString)",
                        ["RQ_1", "0101", "8138308711", 2, sJongmokCode, 100, 0, "03", ""])
                    print("매도")
                    self.isBuy = False
            elif res == -10:
                if self.isBuy == False:
                    # 사용자 구분 요청명 , 화면번호 , 계좌번호, 주문유형, 주식종목코드, 주문수량, 주문단가, 거래 구분, 원주문번호
                    ret = self.kiwoom.dynamicCall(
                        "SendOrder(QString,QString,QString,int,QString,int,int,QString,QString)",
                        ["RQ_1", "0101", "8138308711", 1, sJongmokCode, 100, 0, "03", ""])
                    print("매수")
                    self.isBuy = True
            '''
        elif sRealType =='주식호가잔량':
            # 시간 / 매도호가1 / 매도호가수량1 / 매도호가직전대비1 / 매수호가 / 매수호가수량1 / 매수호가직전대비1 /  ...2~10 / 매도호가총잔량 / 매도호가총잔량직전대비 / 매수호가총잔량 / 매수호가총잔량직전대비
            tm = datetime.now()
            sp = sRealData.split('\t')
            if self.timeDictHo.keys().__contains__(sp[0]):
                self.timeDictHo[sp[0]] = self.timeDictHo[sp[0]] + 1
            else:
                self.timeDictHo[sp[0]] = 1

            cnt = self.timeDictHo[sp[0]]
            sql = 'INSERT INTO STOCK_ASKINGPRICE Values(\'' + str(tm.date()) + " " + sp[0] \
              + '\',' + sp[1]+ ',' + sp[2]+ ',' + sp[4]+ ',' + sp[5] \
              + ',' + sp[7]+ ',' + sp[8]+ ',' + sp[10]+ ',' + sp[11] \
              + ',' + sp[13] + ',' + sp[14] + ',' + sp[16] + ',' + sp[17] \
              + ',' + sp[19] + ',' + sp[20] + ',' + sp[22] + ',' + sp[23] \
              + ',' + sp[25] + ',' + sp[26] + ',' + sp[28] + ',' + sp[29] \
              + ',' + sp[31] + ',' + sp[32] + ',' + sp[34] + ',' + sp[35] \
              + ',' + sp[37] + ',' + sp[38] + ',' + sp[40] + ',' + sp[41] \
              + ',' + sp[43] + ',' + sp[44] + ',' + sp[46] + ',' + sp[47] \
              + ',' + sp[49] + ',' + sp[50] + ',' + sp[52] + ',' + sp[53] \
              + ',' + sp[55] + ',' + sp[56] + ',' + sp[58] + ',' + sp[59] \
              + ',sysdate)'
            self.sqlList.append(sql)

    def ChejangData(self, sGunbun, sNItemCnt, sFidList):
        print("--------- 주식 체결 -----------")
        print(sGunbun)
        print(sNItemCnt)
        print(sFidList)
        if sGunbun == '0':
            '''
            Real Type : 주식체결
            [20] = 체결시간
            [10] = 현재가
            [11] = 전일대비
            [12] = 등락율
            [27] = (최우선)매도호가
            [28] = (최우선)매수호가
            [15] = 거래량
            [13] = 누적거래량
            [14] = 누적거래대금
            [16] = 시가
            [17] = 고가
            [18] = 저가
            [25] = 전일대비기호
            [26] = 전일거래량대비(계약,주)
            [29] = 거래대금증감
            [30] = 전일거래량대비(비율)
            [31] = 거래회전율
            [32] = 거래비용
            [228] = 체결강도
            [311] = 시가총액(억)
            [290] = 장구분
            [691] = KO접근도
            [567] = 상한가발생시간
            [568] = 하한가발생시간
            '''
            #ch = self.kiwoom.dynamicCall("GetChejanData(int)", [10])
            #print(ch)
        elif sGunbun == '4':
            '''
            Real Type : 잔고
            [9201] = 계좌번호
            [9001] = 종목코드,업종코드
            [917] = 신용구분
            [916] = 대출일
            [302] = 종목명
            [10] = 현재가
            [930] = 보유수량
            [931] = 매입단가
            [932] = 총매입가
            [933] = 주문가능수량
            [945] = 당일순매수량
            [946] = 매도/매수구분
            [950] = 당일총매도손일
            [951] = 예수금
            [27] = (최우선)매도호가
            [28] = (최우선)매수호가
            [307] = 기준가
            [8019] = 손익율
            [957] = 신용금액
            [958] = 신용이자
            [918] = 만기일
            [990] = 당일실현손익(유가)
            [991] = 당일실현손익률(유가)
            [992] = 당일실현손익(신용)
            [993] = 당일실현손익률(신용)
            [959] = 담보대출수량
            [924] = Extra Item
            '''
            #종목코드 = self.kiwoom.dynamicCall("GetChejanData(int)", [9001])
            self.current_price = self.kiwoom.dynamicCall("GetChejanData(int)", [10])
            self.meme_price = self.kiwoom.dynamicCall("GetChejanData(int)", [931])
            self.q_wallet = int(self.kiwoom.dynamicCall("GetChejanData(int)", [930]))

            '''
            if len(종목코드) > 6:
                self.map[종목코드[1:7]] = 보유수량
            else:
                self.map[종목코드] = 보유수량
            '''
            #print(종목코드)
            print('현재가 : ',self.current_price,sep='     ')
            print('매입가 : ', self.meme_price, sep='     ')
            print('보유수량 : ',self.q_wallet,sep='     ')

    def ReceiveTrData(self, sScrNo, sRQName, sTRCode, sRecordName, sPreNext, nDataLength, sErrorCode, sMessage, sSPlmMsg, **kwargs):
        #print("ReceiveTrData")
        try:
            print("ReveiveTrData")
            now = datetime.now()
            time = "{}-{}-{} {}:{}:{}".format(now.year, now.month, now.day, now.hour, now.minute, now.second)
            print(sTRCode)
            #if sTRCode =='OPT50029':
            if sTRCode =='####':
                print("분봉 조회")
                # 현재가 / 거래량 / 체결시간 / 시가 / 고가 / 저가
                stick = self.kiwoom.dynamicCall("GetCommDataEx(QString,QString",[sTRCode,"주식분차트"])
                종목코드 = self.kiwoom.dynamicCall("GetCommData(QString,QString,int,QString)",[sTRCode, "계좌평가현황요청", 0, "종목코드"])
                print("종목코드",종목코드)
                df = pd.DataFrame(columns=['현재가','거래량','시가','고가','저가'])
                for data in stick:
                    df.loc[data[2]]={'현재가':data[0].replace('+','').replace('-',''),'거래량':data[1],'시가':data[3].replace('+','').replace('-',''),'고가':data[4].replace('+','').replace('-',''),'저가':data[5].replace('+','').replace('-','')}

                info = MeanMoveLine(data=df)
                print(info.moveLine)
                print(list(info.moveLine)[-1])
                print(list(info.moveLine.values())[-1])
                #info.getDeviation(info.list)
                res = info.currentBuyAndSell(list(info.moveLine.values())[-1],list(info.moveLine.values())[-2])
                print("res:",res)

                ## LOGIC 1

                if res ==10:
                    if self.isBuy == True:

                        #선물옵션 변수
                        t_order = 1        #주문종류  1:신규매매, 2:정정, 3:취소
                        t_meme = 1         #매매구분  1: 매도, 2:매수
                        t_trade = '03'     #거래구분(혹은 호가구분)은아래 참고
                        t_quantity = 1     #주문수량
                        t_meme_price = 0   #주문가격
                        t_order_num =  ""



                        # 사용자 구분 요청명 , 화면번호 , 계좌번호, 주문유형, 주식종목코드, 주문수량, 주문단가, 거래 구분, 원주문번호
                        #// 사용자구분명 // 화면번호 // 계좌번호 // 주문유형 1:신규매수, 2:신규매도 3:매수취소, 4:매도취소, 5:매수정정, 6:매도정정 // 종목코드 // 주문수량 // 주문가격 // 거래구분 // 원주문번호
                        #ret = self.kiwoom.dynamicCall("SendOrder(QString,QString,QString,int,QString,int,int,QString,QString)",["RQ_1", "0101", self.acc, 2, self.jongmok, self.t_quantity, 0, "03", ""])

                        #// 사용자구분명 // 화면번호 // 계좌번호  // 종목코드 // 주문종류  1:신규매매, 2:정정, 3:취소 // 매매구분  1: 매도, 2:매수 // 거래구분(혹은 호가구분)은아래 참고 // 주문수량 // 주문가격 // 원주문번호
                        ret = self.kiwoom.dynamicCall("SendOrderFO(Qstring,Qstring,Qstring,Qstring,int,int,Qstring,int,int,Qstring)", "RQ_1", "0101", self.acc, self.jongmok, t_order , t_meme, t_trade, t_quantity, t_meme_price, t_order_num)



                        print("매도")
                        self.isBuy=False

                elif res == -10:
                    if self.isBuy == False:
                        # 선물옵션 변수
                        t_order = 1  # 주문종류  1:신규매매, 2:정정, 3:취소
                        t_meme = 2  # 매매구분  1: 매도, 2:매수
                        t_trade = '03'  # 거래구분(혹은 호가구분)은아래 참고
                        t_quantity = 1  # 주문수량
                        t_meme_price = 0  # 주문가격
                        t_order_num = ""


                        # 사용자 구분 요청명 , 화면번호 , 계좌번호, 주문유형, 주식종목코드, 주문수량, 주문단가, 거래 구분, 원주문번호
                        #ret = self.kiwoom.dynamicCall("SendOrder(QString,QString,QString,int,QString,int,int,QString,QString)",["RQ_1", "0101", self.acc, 1, self.jongmok, self.t_quantity, 0, "03", ""])
                        ret = self.kiwoom.dynamicCall("SendOrderFO(Qstring,Qstring,Qstring,Qstring,int,int,Qstring,int,int,Qstring)", ["RQ_1", "0101", self.acc, self.jongmok, t_order, t_meme, t_trade, t_quantity, t_meme_price, t_order_num])
                        print("매수")
                        self.isBuy=True


                ## LOGIC2
                '''
                # 5일선이 오르고있을 시
                if list(info.moveLine.values())[-1][2] > list(info.moveLine.values())[-2][2]:
                    if res ==10:
                        if self.isBuy == True:
                            # 사용자 구분 요청명 , 화면번호 , 계좌번호, 주문유형, 주식종목코드, 주문수량, 주문단가, 거래 구분, 원주문번호
                            ret = self.kiwoom.dynamicCall("SendOrder(QString,QString,QString,int,QString,int,int,QString,QString)",["RQ_1", "0101", self.acc, 2, self.jongmok, 10, 0, "03", ""])
                            print("매도")
                            self.isBuy=False
                    elif res == -10:
                        if self.isBuy == False:
                            # 사용자 구분 요청명 , 화면번호 , 계좌번호, 주문유형, 주식종목코드, 주문수량, 주문단가, 거래 구분, 원주문번호
                            ret = self.kiwoom.dynamicCall("SendOrder(QString,QString,QString,int,QString,int,int,QString,QString)",["RQ_1", "0101", self.acc, 1, self.jongmok, 10, 0, "03", ""])
                            print("매수")
                            self.isBuy=True
                # 5일선이 내리고 있을 시
                else:
                    if res ==-10:
                        if self.isBuy == True:
                            # 사용자 구분 요청명 , 화면번호 , 계좌번호, 주문유형, 주식종목코드, 주문수량, 주문단가, 거래 구분, 원주문번호
                            ret = self.kiwoom.dynamicCall("SendOrder(QString,QString,QString,int,QString,int,int,QString,QString)",["RQ_1", "0101", self.acc, 2, self.jongmok, 10, 0, "03", ""])
                            print("매도")
                            self.isBuy=False
                    elif res == 10:
                        if self.isBuy == False:
                            # 사용자 구분 요청명 , 화면번호 , 계좌번호, 주문유형, 주식종목코드, 주문수량, 주문단가, 거래 구분, 원주문번호
                            ret = self.kiwoom.dynamicCall("SendOrder(QString,QString,QString,int,QString,int,int,QString,QString)",["RQ_1", "0101", self.acc, 1, self.jongmok, 10, 0, "03", ""])
                            print("매수")
                            self.isBuy=True
'''
            elif sTRCode == "OPW00004":
                print("계좌")
                보유수량 = int(self.kiwoom.dynamicCall("GetCommData(QString,QString,int,QString)", [sTRCode, "계좌평가현황요청", 0, "보유수량"]))
                종목코드 = self.kiwoom.dynamicCall("GetCommData(QString,QString,int,QString)", [sTRCode, "계좌평가현황요청", 0, "종목코드"])
                self.map[종목코드[1:7]] = 보유수량
                print(종목코드[1:7])
                print(보유수량)

            elif sTRCode == "opw20012":
                print("잔고 - opw20012")
                deposit = self.kiwoom.dynamicCall("GetCommData(QString, QString, int, QString)", sTRCode, sRQName, 0, "인출가능총액")
                print("인출가능총액 : %s" % int(deposit))

                output_deposit = self.kiwoom.dynamicCall("GetCommData(QString,QString,int,QString)", sTRCode, sRQName, 0,
                                                  "주문가능총액")
                print("주문가능총액: %s" % int(output_deposit))



            elif sTRCode == "OPT50029" and sRQName == "선물차트자동매매":
                print(sRQName,sTRCode,sep='     ')
                out_list = ['체결시간', '시가', '고가', '저가', '현재가', '거래량']
                f_result = []
                for i in range(self.chart_request_count):
                    tmp = []
                    j = 0
                    for obj in out_list:
                        # print(self.dynamicCall("GetCommData(QString, QString, int, QString)", sTrCode, sRQName, i, obj))
                        t_data = self.kiwoom.dynamicCall("GetCommData(QString, QString, int, QString)", sTRCode, sRQName, i, obj)
                        t_data = t_data.replace(' ', '', 30)
                        t_data = t_data.replace('+', '', 10)
                        t_data = t_data.replace('-', '', 10)
                        if j > 1 and j < 5:
                            tmp.append(round(float(t_data), 3))
                        else:
                            tmp.append(t_data)
                        j = j + 1
                    f_result.append(tmp)

                df = pd.DataFrame(columns=['일자', '시가', '고가', '저가', '종가', '거래량'], index=range(len(f_result)))
                t_num = 0
                for obj in f_result:
                    df.loc[t_num] = obj
                    t_num = t_num + 1

                df[str(self.t_avr1)] = self.average_price_ext(df['종가'], self.t_avr1)
                df[str(self.t_avr2)] = self.average_price_ext(df['종가'], self.t_avr2)
                df[str(self.t_avr3)] = self.average_price_ext(df['종가'], self.t_avr3)
                df['이격도'] = self.differentical_percent(df['종가'],df['시가'],df[str(self.t_avr2)])
                df['이평선차이'] = self.average_price_delta(df[str(self.t_avr2)], df[str(self.t_avr3)])
                df['양봉확인'] = self.average_price_delta(df['종가'], df['시가'])
                df[str(self.t_avr2)+'일선 기울기'] = self.average_price_slope(df[str(self.t_avr2)])

                print(['일자', '시가', '고가', '저가', '종가', '거래량',str(self.t_avr1),str(self.t_avr2),str(self.t_avr3),'이격도','이평선차이','양봉확인',str(self.t_avr2)+'일선 기울기'])
                for i in df.index:
                    print(list(df.loc[i]))

                self.p_differentical_percent = df['이격도'][0]
                self.p_average_price_delta = df['이평선차이'][0]
                self.p_end_start_price_delta = df['양봉확인'][0]
                self.p_average_price_slope = df[str(self.t_avr2)+'일선 기울기'][0]





            elif sTRCode == "OPT50029":
                print('sRQName : ', sRQName)
                print("선물옵션분차트요청 - OPT50029")
                out_list = ['체결시간', '시가', '고가', '저가', '현재가', '거래량']
                f_result = []
                for i in range(self.chart_request_count):
                    tmp = []
                    j = 0
                    for obj in out_list:
                        # print(self.dynamicCall("GetCommData(QString, QString, int, QString)", sTrCode, sRQName, i, obj))
                        t_data = self.kiwoom.dynamicCall("GetCommData(QString, QString, int, QString)", sTRCode, sRQName, i, obj)
                        t_data = t_data.replace(' ', '', 30)
                        t_data = t_data.replace('+', '', 10)
                        t_data = t_data.replace('-', '', 10)
                        if j > 1 and j < 5:
                            tmp.append(round(float(t_data), 3))
                        else:
                            tmp.append(t_data)
                        j = j + 1
                    f_result.append(tmp)

                df = pd.DataFrame(columns=['일자', '시가', '고가', '저가', '종가', '거래량'], index=range(len(f_result)))
                t_num = 0
                for obj in f_result:
                    df.loc[t_num] = obj
                    t_num = t_num + 1

                df[str(self.t_avr1)] = self.average_price_ext(df['종가'], self.t_avr1)
                df[str(self.t_avr2)] = self.average_price_ext(df['종가'], self.t_avr2)
                df[str(self.t_avr3)] = self.average_price_ext(df['종가'], self.t_avr3)
                df['이격도'] = self.differentical_percent(df['종가'],df['시가'],df[str(self.t_avr2)])
                df['이평선차이'] = self.average_price_delta(df[str(self.t_avr2)], df[str(self.t_avr3)])
                df['양봉확인'] = self.average_price_delta(df['종가'], df['시가'])
                df[str(self.t_avr2)+'일선 기울기'] = self.average_price_slope(df[str(self.t_avr2)])

                print(['일자', '시가', '고가', '저가', '종가', '거래량',str(self.t_avr1),str(self.t_avr2),str(self.t_avr3),'이격도','이평선차이','양봉확인',str(self.t_avr2)+'일선 기울기'])
                for i in df.index:
                    print(list(df.loc[i]))

                self.p_differentical_percent = df['이격도'][0]
                self.p_average_price_delta = df['이평선차이'][0]
                self.p_end_start_price_delta = df['양봉확인'][0]
                self.p_average_price_slope = df[str(self.t_avr2)+'일선 기울기'][0]

                #print(self.p_differentical_percent,self.p_average_price_delta,self.p_end_start_price_delta,self.p_average_price_slope)

            elif sTRCode == "OPT50028":

                print("선물틱차트요청 - OPT50028")
                out_list = ['체결시간', '현재가', '거래량']
                f_result = []
                for i in range(self.tic_chart_request_count):
                    tmp = []
                    j = 0
                    for obj in out_list:
                        # print(self.dynamicCall("GetCommData(QString, QString, int, QString)", sTrCode, sRQName, i, obj))
                        t_data = self.kiwoom.dynamicCall("GetCommData(QString, QString, int, QString)", sTRCode,
                                                         sRQName, i, obj)
                        t_data = t_data.replace(' ', '', 30)
                        t_data = t_data.replace('+', '', 10)
                        t_data = t_data.replace('-', '', 10)
                        if j > 1 and j < 5:
                            tmp.append(round(float(t_data), 3))
                        else:
                            tmp.append(t_data)
                        j = j + 1
                    f_result.append(tmp)

                df = pd.DataFrame(columns=['일자', '종가', '거래량'], index=range(len(f_result)))
                t_num = 0
                for obj in f_result:
                    df.loc[t_num] = obj
                    t_num = t_num + 1

                #df[str(self.t_avr1)] = self.average_price_ext(df['종가'], self.t_avr1)
                #df[str(self.t_avr2)] = self.average_price_ext(df['종가'], self.t_avr2)
                #df[str(self.t_avr3)] = self.average_price_ext(df['종가'], self.t_avr3)
                #df['이격도'] = self.differentical_percent(df['종가'], df['시가'], df[str(self.t_avr2)])
                #df['이평선차이'] = self.average_price_delta(df[str(self.t_avr2)], df[str(self.t_avr3)])
                for i in df.index:
                    print(list(df.loc[i]))




                #self.detail_account_info_event_loop_f.exit()

            elif sTRCode == "opw20006":
                final_ratio = 1*0.01*0.003
                print("선옵잔고상세현황요청 - opw20006")
                m_code = self.kiwoom.dynamicCall("GetCommData(QString, QString, int, QString)", sTRCode, sRQName, 0, "종목코드")
                m_time = self.kiwoom.dynamicCall("GetCommData(QString, QString, int, QString)", sTRCode, sRQName, 0,
                                               "매매일자")
                try:
                    q_contract = int(self.kiwoom.dynamicCall("GetCommData(QString, QString, int, QString)", sTRCode, sRQName, 0,
                                                 "잔고수량"))

                    m_now_price = int(self.kiwoom.dynamicCall("GetCommData(QString, QString, int, QString)", sTRCode, sRQName, 0,
                                                 "평가금액"))
                    m_origin_price = int(self.kiwoom.dynamicCall("GetCommData(QString, QString, int, QString)", sTRCode, sRQName, 0,
                                                 "매매금액"))

                    #self.current_price = self.kiwoom.dynamicCall("GetCommData(QString, QString, int, QString)", sTRCode, sRQName, 0, "매입단가")
                    self.current_price = round(int(self.kiwoom.dynamicCall("GetCommData(QString, QString, int, QString)", sTRCode, sRQName, 0, "매입단가"))/100,2)

                    f_profit = int((m_now_price - m_origin_price) - (m_now_price + m_origin_price) * final_ratio)
                    print("매매일자 : ",m_time," ## 종목코드 : ",m_code,' ## 잔고수량 : ', q_contract ," ## 평가손익 : ",f_profit, ' ## 매입단가 : ', self.current_price, sep= '')

                except:
                    print("보유수량없음")


            elif sTRCode == "opt50001":
                self.current_price = self.kiwoom.dynamicCall("GetCommData(QString, QString, int, QString)", sTRCode, sRQName, 0, "매수호가1")
                print(self.current_price)
                self.mesu1_price = self.current_price
                print("요청종료 : ", datetime.now())

            elif sTRCode == "opt10075":
                self.order_num = self.kiwoom.dynamicCall("GetCommData(QString, QString, int, QString)", sTRCode, sRQName, 0, "주문번호")



        except Exception as e:
            print(e)
            print(traceback.format_exc())

    # 보유 수량 조회
    def searchHowMany(self):
        #self.kiwoom.dynamicCall("SetInputValue(QString,QString)", "계좌번호", )
        #self.kiwoom.dynamicCall("SetInputValue(QString,QString)", "비밀번호", )
        #self.kiwoom.dynamicCall("SetInputValue(QString,QString)", "상장폐지조회구분", 0)
        #self.kiwoom.dynamicCall("CommRqData(QString,QString,int,QString)", ["RQ_NAME", "OPW00004", 0, "0101"])
        pass
    def login(self):
        self.kiwoom.dynamicCall("CommConnect()")
        print("로그인이 완료되었습니다")

    def auto(self):
        jongmok = self.jongmok
        #x = Thread(target=self.sam, args=('005930',))
        #x.start()
        #self.kiwoom.dynamicCall("SetInputValue(QString,QString)", "종목코드", jongmok)
        #self.kiwoom.dynamicCall("CommRqData(QString,QString,int,QString)", ["RQ_NAME", "OPT10001", 0, "0101"])
        x1 = Thread(target=self.sam, args=(jongmok,))
        x1.start()

    def sam(self,jongmok):
        print('프로그램시작')
        while True:
            now=datetime.now()
            if now.second == 0 or now.second == 30:
                print("분봉")
                self.kiwoom.dynamicCall("SetInputValue(QString,QString)", "종목코드", jongmok)
                self.kiwoom.dynamicCall("CommRqData(QString,QString,int,QString)", ["RQ_NAME", "OPT50029", 0, "0101"])
            time.sleep(1)

        '''
        self.searchHowMany()
        self.mutex.acquire()
        cur = self.conn.cursor()
        cur.execute("UPDATE stock_status SET status=0, update_tm = sysdate where jongmok ='"+jongmok+"'")
        self.conn.commit()
        cur.close()
        self.mutex.release()
        self.kiwoom.dynamicCall("SetInputValue(QString,QString)", "종목코드", jongmok)

        self.kiwoom.dynamicCall("CommRqData(QString,QString,int,QString)", ["RQ_NAME", "OPT10004", 0, "0101"])

        while True:
            print("check")
            self.mutex.acquire()
            cur = self.conn.cursor()
            cur.execute("select status,price from stock_status where jongmok ='"+jongmok+"'")
            self.mutex.release()
            for row in cur:
                st = row[0]
                price = row[1]

            if st == 0:
                pass

            elif st == 1:
                if self.map[self.jongmok]== 0:
                    # 사용자 구분 요청명 , 화면번호 , 계좌번호, 주문유형, 주식종목코드, 주문수량, 주문단가, 거래 구분, 원주문번호
                    ret = self.kiwoom.dynamicCall("SendOrder(QString,QString,QString,int,QString,int,int,QString,QString)",
                                                  ["RQ_1", "0101", "8138308711", 1, self.jongmok, 5, 0, "03", ""])
                    self.mutex.acquire()
                    cur = self.conn.cursor()
                    cur.execute("UPDATE stock_status SET status=0, update_tm = sysdate where jongmok ='" + jongmok + "'")
                    self.conn.commit()
                    cur.close()
                    self.mutex.release()
                    print("매수")

            elif st == 2:
                if self.map[self.jongmok] > 0:
                    ret = self.kiwoom.dynamicCall("SendOrder(QString,QString,QString,int,QString,int,int,QString,QString)",["RQ_1", "0101", "8138308711", 2, jongmok, 5, 0, "03", ""])
                    self.mutex.acquire()
                    cur = self.conn.cursor()
                    cur.execute("UPDATE stock_status SET status=0, update_tm = sysdate where jongmok ='" + jongmok + "'")
                    self.conn.commit()
                    cur.close()
                    self.mutex.release()
                    print("매도")
            time.sleep(1)
        '''
    def dbInsertThread(self):
        print("dbInsert Thread Start")

        while True:
            #print(len(self.sqlList))
            self.mutex.acquire()
#            cursor = self.conn.cursor()
            list = self.sqlList[:]
            self.sqlList.clear()
            for data in list:
                try:
                    sql = data
                    print(sql)
#                    cursor.execute(sql)
#                    self.conn.commit()
                except Exception as e:
                    print(e)
                    print(data)
                    self.sqlList.append(data)

#           self.conn.commit()
#            cursor.close()
            self.mutex.release()
            time.sleep(1)

    def remain(self):
        #print("hi hi")
        #print(self.acc)
        self.kiwoom.dynamicCall("SetInputValue(QString, QString)", "계좌번호", self.acc)
        self.kiwoom.dynamicCall("SetInputValue(QString, QString)", "비밀번호", "")
        self.kiwoom.dynamicCall("SetInputValue(QString, QString)", "비밀번호입력매체구분", "00")
        self.kiwoom.dynamicCall("CommRqData(QString, QString, int, QString)", "선옵증거금상세내역요청", "opw20012", 0, "0101")
        #print("bye bye")


        #self.kiwoom.dynamicCall("SetInputValue(QString, QString)", "계좌번호", acc)

        #self.kiwoom.dynamicCall("CommRqData(QString,QString,int,QString)", ["RQ_NAME", "OPW00004", 0, "0101"])

    def future_chart(self):
        #print('hi')
        print("종목코드 : ", self.jongmok)
        self.kiwoom.dynamicCall("SetInputValue(QString, QString)", "종목코드", self.jongmok)
        self.kiwoom.dynamicCall("SetInputValue(QString, QString)", "시간단위", "1")
        #self.dynamicCall("CommRqData(QString, QString, int, QString)", "선물옵션분차트요청", "OPT50029", sPrevNext, self.screen_call_price)
        self.kiwoom.dynamicCall("CommRqData(QString, QString, int, QString)", "선물옵션분차트요청", "OPT50029", "0",  "7000")
        #self.detail_account_info_event_loop_f.exec_()
        #print('bye')

    def future_chart_auto(self):
        #print('hi')
        print("종목코드 : ", self.jongmok)
        self.kiwoom.dynamicCall("SetInputValue(QString, QString)", "종목코드", self.jongmok)
        self.kiwoom.dynamicCall("SetInputValue(QString, QString)", "시간단위", "1")
        #self.dynamicCall("CommRqData(QString, QString, int, QString)", "선물옵션분차트요청", "OPT50029", sPrevNext, self.screen_call_price)
        self.kiwoom.dynamicCall("CommRqData(QString, QString, int, QString)", "선물차트자동", "OPT50029", "0",  "7000")


    def future_tic_chart(self):
        #print('hi')
        print("종목코드 : ", self.jongmok)
        self.kiwoom.dynamicCall("SetInputValue(QString, QString)", "종목코드", self.jongmok)
        self.kiwoom.dynamicCall("SetInputValue(QString, QString)", "시간단위", "1")
        #self.dynamicCall("CommRqData(QString, QString, int, QString)", "선물옵션분차트요청", "OPT50029", sPrevNext, self.screen_call_price)
        self.kiwoom.dynamicCall("CommRqData(QString, QString, int, QString)", "선물틱차트요청", "OPT50028", "0",  "7000")
        #self.detail_account_info_event_loop_f.exec_()
        #print('bye')

    #봉 이평선
    def num_to_str(sefl,t_int):
        if t_int > 9:
            return str(t_int)
        else:
            return '0' + str(t_int)



    def average_price_ext(self, t_df, t_date):
        if t_date == 1:
            return t_df
        else:
            r_data = pd.DataFrame(index=list(range(0, len(t_df))), columns=list('a'))
            for i in range(len(t_df)-t_date+1):
                r_data['a'][i] = round(t_df[i:i + t_date].mean(),2)
                #print(r_data['a'][i])

            return r_data['a']

    def differentical_percent(self,t_df1,t_df2,t_df3):
        r_data = pd.DataFrame(index=list(range(0, len(t_df1))), columns=list('a'))

        for i in range(len(t_df1)):
            #print(type(t_df1[i]),type(t_df2[i]),type(t_df3[i]),i)
            r_data['a'][i] = round(100*0.5*(t_df1[i]+float(t_df2[i]))/t_df3[i],5)
            # print(r_data['a'][i])

        return r_data['a']

    def average_price_delta(self,t_df1,t_df2):
        r_data = pd.DataFrame(index=list(range(0, len(t_df1))), columns=list('a'))

        for i in range(len(t_df1)):
            #print(type(t_df1[i]),type(t_df2[i]),i)
            r_data['a'][i] = round(float(t_df1[i])-float(t_df2[i]),2)
            # print(r_data['a'][i])

        return r_data['a']

    def average_price_slope(selfs,t_df):
        r_data = pd.DataFrame(index=list(range(0, len(t_df))), columns=list('a'))
        for i in range(len(t_df) -1):
            r_data['a'][i] = round(t_df[i]-t_df[i+1], 2)
            # print(r_data['a'][i])
        #r_data['a'][len(t_df) -1] = np.nan
        return r_data['a']


    def asset_check(self):
        r = datetime.now()
        t_info = str(r.year) + self.num_to_str(r.month) + self.num_to_str(r.day)

        self.kiwoom.dynamicCall("SetInputValue(QString, QString)", "계좌번호", self.acc)
        self.kiwoom.dynamicCall("SetInputValue(QString, QString)", "비밀번호", "")


        #YYYYMMDD
        self.kiwoom.dynamicCall("SetInputValue(QString, QString)", "조회일자", t_info)
        self.kiwoom.dynamicCall("SetInputValue(QString, QString)", "비밀번호입력매체구분", "00")
        # self.dynamicCall("CommRqData(QString, QString, int, QString)", "선물옵션분차트요청", "OPT50029", sPrevNext, self.screen_call_price)
        self.kiwoom.dynamicCall("CommRqData(QString, QString, int, QString)", "선옵잔고상세현황요청", "opw20006", "0", "8000")

    def f_buy(self):
        print(datetime.now())
        # 선물옵션 변수
        t_order = 1  # 주문종류  1:신규매매, 2:정정, 3:취소
        t_meme = 2  # 매매구분  1: 매도, 2:매수
        t_trade = '3'  # 시장가 거래구분(혹은 호가구분)은아래 참고
        t_quantity = 1  # 주문수량
        t_meme_price = "0"  # 주문가격
        t_order_num = ""



        # 사용자 구분 요청명 , 화면번호 , 계좌번호, 주문유형, 주식종목코드, 주문수량, 주문단가, 거래 구분, 원주문번호
        #ret = self.kiwoom.dynamicCall("SendOrder(QString,QString,QString,int,QString,int,int,QString,QString)",["RQ_1", "0101", self.acc, 1, self.jongmok, self.t_quantity, 0, "03",""])

        ret = self.kiwoom.dynamicCall("SendOrderFO(Qstring,Qstring,Qstring,Qstring,int,Qstring,Qstring,int,Qstring,Qstring)",
                                     ["RQ_1", "0101", self.acc, self.jongmok, t_order, t_meme, t_trade, t_quantity,
                                       t_meme_price, t_order_num])
        #print(ret)
        print("매수주문")
        if ret != 0:
            print("주문실패")
            self.KiwoomProcessingError("SendOrder : " + ReturnCode.CAUSE[ret])
        else:
            print("주문성공")
        self.isBuy = True

    def f_buy_fix_price(self):
        self.current_price_call()
        print(datetime.now())
        # 선물옵션 변수
        t_order = 1  # 주문종류  1:신규매매, 2:정정, 3:취소
        t_meme = 2  # 매매구분  1: 매도, 2:매수
        t_trade = '0'  # 시장가 거래구분(혹은 호가구분)은아래 참고 / 지정가로 매수
        t_quantity = 1  # 주문수량
        if '+' in self.current_price:
            t_meme_price = self.current_price.split('+')[1]  # 주문가격
        else:
            t_meme_price = self.current_price.split('-')[1]  # 주문가격
        t_order_num = ""
        print("현재가격 : ",t_meme_price,sep = '        ')


        # 사용자 구분 요청명 , 화면번호 , 계좌번호, 주문유형, 주식종목코드, 주문수량, 주문단가, 거래 구분, 원주문번호
        #ret = self.kiwoom.dynamicCall("SendOrder(QString,QString,QString,int,QString,int,int,QString,QString)",["RQ_1", "0101", self.acc, 1, self.jongmok, self.t_quantity, 0, "03",""])

        ret = self.kiwoom.dynamicCall("SendOrderFO(Qstring,Qstring,Qstring,Qstring,int,Qstring,Qstring,int,Qstring,Qstring)",
                                     ["RQ_1", "0101", self.acc, self.jongmok, t_order, t_meme, t_trade, t_quantity,
                                       t_meme_price, t_order_num])
        #print(ret)
        print("매수주문")
        if ret != 0:
            print("주문실패")
            self.KiwoomProcessingError("SendOrder : " + ReturnCode.CAUSE[ret])
        else:
            print("주문성공")
        self.isBuy = True


    def f_sell(self):
        print(datetime.now())
        # 선물옵션 변수
        t_order = 1  # 주문종류  1:신규매매, 2:정정, 3:취소
        t_meme = 1  # 매매구분  1: 매도, 2:매수
        t_trade = '3'  # 거래구분(혹은 호가구분)은아래 참고
        t_quantity = 1  # 주문수량
        t_meme_price = 0  # 주문가격
        t_order_num = ""

        # 사용자 구분 요청명 , 화면번호 , 계좌번호, 주문유형, 주식종목코드, 주문수량, 주문단가, 거래 구분, 원주문번호
        # // 사용자구분명 // 화면번호 // 계좌번호 // 주문유형 1:신규매수, 2:신규매도 3:매수취소, 4:매도취소, 5:매수정정, 6:매도정정 // 종목코드 // 주문수량 // 주문가격 // 거래구분 // 원주문번호
        # ret = self.kiwoom.dynamicCall("SendOrder(QString,QString,QString,int,QString,int,int,QString,QString)",["RQ_1", "0101", self.acc, 2, self.jongmok, self.t_quantity, 0, "03", ""])
        # // 사용자구분명 // 화면번호 // 계좌번호  // 종목코드 // 주문종류  1:신규매매, 2:정정, 3:취소 // 매매구분  1: 매도, 2:매수 // 거래구분(혹은 호가구분)은아래 참고 // 주문수량 // 주문가격 // 원주문번호
        ret = self.kiwoom.dynamicCall("SendOrderFO(Qstring,Qstring,Qstring,Qstring,int,int,Qstring,int,Qstring,Qstring)",
                                      ["RQ_1", "0101", self.acc, self.jongmok, t_order, t_meme, t_trade, t_quantity,
                                       t_meme_price, t_order_num])
        #print(ret)
        print("매도주문")
        if ret != 0:
            print("주문실패")
            self.KiwoomProcessingError("SendOrder : " + ReturnCode.CAUSE[ret])
        else:
            print("주문성공")
        self.isBuy = False


    def reverse_sell(self):
        '''


        :return:
        '''











    def f_cancel(self):
            self.non_contract()
            # 선물옵션 변수
            t_order = 3  # 주문종류  1:신규매매, 2:정정, 3:취소
            t_meme = 1  # 매매구분  1: 매도, 2:매수
            t_trade = '3'  # 거래구분(혹은 호가구분)은아래 참고
            t_quantity = 1  # 주문수량
            t_meme_price = 0  # 주문가격
            t_order_num = self.order_num

            # 사용자 구분 요청명 , 화면번호 , 계좌번호, 주문유형, 주식종목코드, 주문수량, 주문단가, 거래 구분, 원주문번호
            # // 사용자구분명 // 화면번호 // 계좌번호 // 주문유형 1:신규매수, 2:신규매도 3:매수취소, 4:매도취소, 5:매수정정, 6:매도정정 // 종목코드 // 주문수량 // 주문가격 // 거래구분 // 원주문번호
            # ret = self.kiwoom.dynamicCall("SendOrder(QString,QString,QString,int,QString,int,int,QString,QString)",["RQ_1", "0101", self.acc, 2, self.jongmok, self.t_quantity, 0, "03", ""])
            # // 사용자구분명 // 화면번호 // 계좌번호  // 종목코드 // 주문종류  1:신규매매, 2:정정, 3:취소 // 매매구분  1: 매도, 2:매수 // 거래구분(혹은 호가구분)은아래 참고 // 주문수량 // 주문가격 // 원주문번호
            ret = self.kiwoom.dynamicCall(
                "SendOrderFO(Qstring,Qstring,Qstring,Qstring,int,int,Qstring,int,Qstring,Qstring)",
                ["RQ_1", "0101", self.acc, self.jongmok, t_order, t_meme, t_trade, t_quantity,
                 t_meme_price, t_order_num])
            print(ret)
            print("취소")
            self.isBuy = False



    def non_contract(self):
        self.kiwoom.dynamicCall("SetInputValue(QString, QString)","계좌번호", self.acc)
        self.kiwoom.dynamicCall("SetInputValue(QString, QString)","전체종목구분", "0")
        self.kiwoom.dynamicCall("SetInputValue(QString, QString)","매매구분", "0")
        self.kiwoom.dynamicCall("SetInputValue(QString, QString)","종목코드", self.jongmok)
        self.kiwoom.dynamicCall("SetInputValue(QString, QString)","체결구분", "1")
        self.kiwoom.dynamicCall("CommRqData(QString, QString, int, QString)","RQName", "opt10075", "0", "1200")


    def current_price_call(self):
        print("요청시작 : ", datetime.now())
        self.kiwoom.dynamicCall("SetInputValue(QString, QString)", "종목코드", self.jongmok)
        self.kiwoom.dynamicCall("CommRqData(QString, QString, int, QString)", "선옵현재가정보요청", "opt50001", "0", "1001")

    def future_chart_1s(self):
        print("hi")
        x1 = Thread(target=self.future_chart_1s_exe, args=())
        #self.x1.daemon = True
        x1.start()
        print("bye")


    def future_chart_1s_exe(self):
        while True:
            try:
                print(time.time())
                self.future_chart()
                time.sleep(1)
            except:
                pass
    #except KeyboardInterrupt:
    #        print("차트종료")
    #        pass





if __name__ == "__main__":
    app = QApplication(sys.argv)

    myWindow = MyWindow()
    #x = Thread(target=myWindow.dbInsertThread, args=())
    #x.daemon = True
    #x.start()
    myWindow.show()
    app.exec_()